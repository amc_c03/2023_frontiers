# Monolithic AM Façade: multi-objective parametric design optimization of additively manufactured insulating wall elements

### [doi: 10.3389/fbuil.2023.1286933](https://doi.org/10.3389/fbuil.2023.1286933)

## Authors
David Briels <sub>1*</sub>, Mauritz Renz <sub>1</sub>, Ahmad Nouman <sub>1</sub>,  Alexander Straßer <sub>2</sub>, Maximilian Hechtl <sub>2</sub>, Maximilian Dahlenburg <sub>3</sub>, Bruno Knychalla <sub>4</sub>, Patrick Sonnleitner <sub>4</sub>, Friedrich Herding <sub>5</sub>, Julia Fleckenstein <sub>6</sub>, Ema Krakovská <sub>6</sub>, Kathrin Dörfler <sub>6</sub>, Thomas Auer <sub>1</sub> 

## Affiliations
<sub>1</sub> Chair of Building Technology and Climate Responsive Design, TUM School of Engineering and Design, Technical University of Munich, Munich, Germany

<sub>2</sub> Chair of Materials Science and Testing, TUM School of Engineering and Design, Technical University of Munich, Munich, Germany

<sub>3</sub> Chair of Materials Handling, Material Flow, Logistics, TUM School of Engineering and Design, Technical University of Munich, Munich, Germany

<sub>4</sub> additive tectonics GmbH, Lupburg, Germany

<sub>5</sub> Institute of Building Materials, Concrete Construction and Fire Safety, Technische Universität Braunschweig, Braunschweig, Germany

<sub>6</sub> TT Professorship Digital Fabrication, TUM School of Engineering and Design, Technical University of Munich, Munich, Germany

## Abstract
Additive Manufacturing (AM) offers transformative opportunities to create functionally hybridized, insulating, monolithic AM wall elements. The novel fabrication methods of AM allow for the production of highly differentiated building components with intricate internal and external geometries, aiming for reduced material use while integrating and enhancing building performance features including thermal insulation performance. This study focuses on integrating such thermal insulation performance by leveraging the individual features of three distinct AM processes: Selective Paste Intrusion (SPI), Selective Cement Activation (SCA), and Extrusion 3D Concrete Printing (E3DCP). Using a simulation-based parametric design approach, this research investigates 4,500 variations of monolithic AM façade elements derived from a generative hexagonal cell layout with differing wall widths, the three respective AM processes, different material compositions with and without lightweight aggregates, and three different insulation strategies, namely air-filled cells, encapsulated lightweight aggregates, and additional insulation material within the cavities. Thermal performance feedback is realized via 2D heat flux simulations embedded into a parametric design workflow, and structural performance is considered in a simplified way via geometric and material-specific evaluation. The overall research goal is a multi-objective design optimization, particularly identifying façade configurations that achieve a U-value of less than 0.28 W/m2K and a theoretical compressive strength exceeding 2.70 MN per meter wall length. The results of this study detect 7 % of all generated variations in line with these thermal and structural requirements. 

# Reserch Data Repository
## Purpose of the Repository
This repository enables public access to the software tool and the raw simulation results of this research. It aims to promote transparency, reproducibility, and further research in the domain.

## Description
This repository contains result data collected from simulations for the research paper titled "Monolithic AM Façade: multi-objective parametric design optimization of additively manufactured insulating wall elements." The `data.csv` file contains the simulation results, which can be accessed via Design Explorer for data visualization and analysis. The data can be used for further research, validation, and exploration. Furthermore, the repository contains the Grasshopper tool designed for parametric modeling of the cell geometry and running the simulation-based parametric study. 

## Repository Structure
- `code/MonolithicAMFacade.gh`: Grasshopper script with the Tool
- `data/data.csv`: Contains the result data collected from the simulation-based parametric study
- `data/MonolithicAMFacade_input.xlsx`: Contains the input data for geometry and materials
- `images`: Contains images of design alterations
- `README.md`: This documentation file


# Software Tool

## Authors
- David Briels <sub>1*
- Mauritz Renz <sub>1
- Ahmad Nouman <sub>1
- Thomas Auer <sub>1

## Affiliation
<sub>1</sub> Chair of Building Technology and Climate Responsive Design, TUM School of Engineering and Design, Technical University of Munich, Munich, Germany

## Main Features

With the help of the Grasshopper algorithm, one can autonomously create a hexagonal cell pattern with varying geometric parameters and material attributes sourced from an Excel database. The script furthermore enables a simulation-based parametric study using bulk thermal conductivity simulations across many distinct material and geometry combinations. Additionally, it quantifies and archives other pertinent physical properties such as mass, relative density, and a theoretical compressive strength for each generated variation. 

## Requirements

- Rhinoceros V 6.0 [Download](https://www.rhino3d.com/download/)
- Grasshopper Plugins:
    - Ladybug 0.0.68 [Legacy Plugins](https://www.food4rhino.com/en/app/ladybug-tools)
    - TT Toolbox 2 [Download](https://www.food4rhino.com/en/app/tt-toolbox)
    - Lunchbox V2 [Download](https://www.food4rhino.com/en/app/lunchbox)
    - Clipper 0.3.3 [Download](https://www.food4rhino.com/en/app/clipper-grasshopper-and-rhino)
- LBNL THERM 7.6 [Download](https://windows.lbl.gov/therm-software-downloads)

## Workflow

### Excel

The Excel file contains all the relevant information on material properties and desired combinations. Worksheet 1 showcases different materials in each column, grouped by process. Worksheet 2 lists the desired variant combinations. The material names must match precisely to automatically align in Grasshopper later. It is crucial to retain the structure; more rows can be added.


### Grasshopper

#### Setup

1. Make sure to have all software and plugins installed.
c2. Open a new Rhino 6 file. Use the command "units" to set the unit to "mm" and the tolerance to "0.00001". Set a "Top" view in the display options to "Render".
3. Start Grasshopper and import the Grasshopper file. All yellow groups require adjustment before use.
4. Set the correct file path for the material-properties Excel file in "II Material Properties → 1 Import Excel table."
5. Specify an output file path for the "Colibri Aggregator" in "I Colibri → 4 Output."
6. Set "Colibri" and "Therm" to TRUE in "I Colibri → 3 Control Center". Right-click the Iterator in "I Colibri → 3 Control Center" and press "Fly Test". Check the specified output folders. If everything works, reset the DataRecorders in "I Colibri → 6 Error Counter" and start the Iterator by pressing "Fly."

#### I Colibri

1. **Image Settings**: References a view in Rhino and selects the image size in px.
2. **Variable Inputs**: Contains all parameters that change during the simulation. Sliders, Value Lists, and Panels are permitted. The "Selection" panel specifies the simulation divisions in the given range of the sliders. The order of inputs matches.
3. **Control Center**: The "Colibri" Boolean Toggle determines whether Colibri saves the simulated results while running. The "Therm" Boolean Toggle enables or disables the synchronous simulation while Colibri iterates through them.
4. **Output**: The "Colibri Aggregator" gathers all relevant Input parameters in the Input "Genome" and all resulting calculations in the Input "Phenome", and saves these in a CSV table at the specified file path.
5. **Genome Name**: Concatenates all Input parameters to form a unique ID used to save the corresponding Therm file.
6. **Error Counter**: The recorders save all simulated datasets, and the cluster counts the total number of simulations and the number of failed simulations. It also displays both values and formats the percentage of failed simulations. Reset the counter by pressing the "x" on them before each simulation.

#### II Material Properties

1. **Import Excel Table**: Imports the Excel table from your specified file path.
2. **Variant Numbers, Variant Names**: Extracts the correct worksheets and sends all variants to "I Colibri" as a list of variants.
3. **Query Material Properties for Selected Variants**: Takes the selected variant from the Iterator in "I Colibri" and selects the specified combination of materials. Returns a tree with one branch for each material, including all specified material properties from the Excel table.

#### III Geometry

1. **Generate Pattern**: Takes the defined parameters from "I Colibri" to generate a hexagonal pattern using the "TT Lunchbox" plugin.
2. **Offset CWT**: Offsets each generated cell curve inwards by half of the cell wall thickness [CWT].
3. **Rebuild CRV**: Maintains only vertices that define corners.
4. **Restore the Original Order After the Clipper Offset**: Rearranges the offset cell curves in their original order.
5. **Filter Corners**: Sorts out incongruent cases between the original and offset curves.
6. **Cull Small Cells**: Eliminates all small cells with a surface below 100 square millimeters.
7. **Generate Cell Wall Mesh**: Divides the single surface into triangles.
8. **Extract Boundaries**: Extracts the interior and exterior boundary curves.
9. **Offset CWT Borders**: Generates a vector in the heat flux direction.
10. **Generate Borders**: Adds an interior and exterior covering border.
11. **Preview**: Gathers all necessary geometrical elements for preview.

#### IV Therm

1. **Create THERM Surfaces**: Assigns thermal conductivity properties to each material.
2. **Create THERM Boundaries**: Assigns interior and exterior name tags and temperatures to the boundary curves.
3. **THERM Simulations**: Uses the THERM Ladybug Plugin to generate THERM-readable XML files.
4. **Preview Heat Flux**: Displays the colored mesh and legend output.

#### V Specifications

1. **U-Value**: Appends the unit to the resulting uFactor and displays it below the colored mesh in Rhino.
2. **Cell Dimensions**: Measures the cell size of each individual cell in u- and v-direction and extracts the minimum and maximum, rounding to a whole number.
3. **Cell Area**: Extracts the total area of all cells and all cell walls, and both combined.
4. **Extract Compressive Strength f_c**: Extracts the compressive strength for the selected material value from the Excel table.
5. **Load Bearing Capacity [LBC]**: Calculates the load-bearing capacity by multiplying the compressive strength with the cell wall area.
6. **Extract Density**: Extracts the density from the Excel table.
7. **Mass**: Calculates the total mass in kg/meter.

# Data Analysis
## Design Explorer

Design Explorer is an open-source platform that facilitates the exploration of complex, multi-dimensional design spaces. The platform is used to visualize and understand how changing variables affect the final design straightforwardly and interactively.

The data can be accessed by pressing "Get Data" in the upper left corner and uploading the data.csv to the second option in the pop-up menu. Design Explorer lists all input and output parameters in separate columns, with their values vertically arranged. Clicking and dragging the cursor on specific values can reduce the selection to the wanted area. This selection can be changed or inspected more closely by the button "Zoom to Selection" in the top bar. This can be reset by the button "Reset Selection" at the very left in the top bar. All remaining selections are shown in the scatter plots in the upper right corner, and the colored meshes are represented with the unique parameters in the lower half.

For this parametric study, Design Explorer serves to quickly focus the simulation set on a specific use case and find the optimal variant. It is easy to find the best U-value in a table, but rapidly filtering by multiple requirements can be challenging. For example, the best U-value for a minimum load-bearing capacity of 7.5 MN/m, a maximum wall thickness of 500 mm, and a mass below 700 kg/m is quickly achieved with this tool. The remaining results can be previewed and examined with all parameters in Design Explorer or replicated in Grasshopper.

## How to Use the Data
1. **Download `data.csv`**: Clone this repository or download the `data.csv` file from the folder 'csv_files' onto your local drive.
2. **Upload to Design Viewer**: Upload the `data.csv` file to the [Design Explorer](https://tt-acm.github.io/DesignExplorer/) for visualization and analysis.
3. In the Design Explorer, click on 'Get Data' on the top left corner. Then choose `data.csv` from your local drive.
4. Allow the images to be loaded by the Design Explorer, usually takes 2 minutes or less.

# License
This dataset and all its contents are made available under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/). This means you are free to:

- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material

For any purpose, even commercially, as long as you give appropriate credit, provide a link to the license, and indicate if changes were made.

# Citation
If you use the contents of this repository for your research, please cite the following paper:

Briels D, Renz M, Nouman AS, Straßer A, Hechtl M, Dahlenburg M, Knychalla B, Sonnleitner P, Herding F, Fleckenstein J, Krakovská E, Dörfler K and Auer T (2023),
Monolithic AM façade: multi-objective parametric design optimization of additively manufactured insulating wall elements. Front. Built Environ. 9:1286933. [doi: 10.3389/fbuil.2023.1286933](https://doi.org/10.3389/fbuil.2023.1286933)

# Contact Information
For any further queries or clarifications, feel free to reach out to the lead author:

**David Briels** 
- Email: [david.briels@tum.de](mailto:david.briels@tum.de)
  
